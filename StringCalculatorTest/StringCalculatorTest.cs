﻿using System;
using StringCalculatorV20;
using NUnit.Framework;

namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private IStringCalculator _calculator;

        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 0;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenOneNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 1;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenUnlimitedNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Add("1,2,3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenNewLineAsDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;
            _calculator = new StringCalculator();

            //Act
            var actual = _calculator.Add("1\n2,3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenCustomDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Add("//;\n1;2");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenNegativeNumbers_WhenAdding_ThenThrowException()
        {
            //Arrange
            var expected = "Negatives not allowed. -1 -2";
            _calculator = new StringCalculatorHandler();

            //Act
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//;\n-1;-2"));

            //Assert
            Assert.AreEqual(expected, exception.Message);
        }

        [Test]
        public void GivenNumbers_WhenAdding_ThenIgnoreNumbesGreaterThousand()
        {
            //Arrange
            var expected = 3;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Add("//;\n1;2;1001");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenAnyLengthDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Add("//***\n1***2***3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenMultipleDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Add("//[*][#]\n1*2#3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GivenInvalidDelimiters_WhenAdding_ThenThrowException()
        {
            //Arrange
            var expected = "Invalid Delimiters";
            _calculator = new StringCalculatorHandler();

            //Act
            var exception = Assert.Throws<Exception>(() => _calculator.Add("//;\n-1&-2"));

            //Assert
            Assert.AreEqual(expected, exception.Message);
        }

        [Test]
        public void GivenYnlimitedMultipleDelimiters_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;
            _calculator = new StringCalculatorCustomDelimiter();

            //Act
            var actual = _calculator.Add("//[**$][#%%]\n1**$2#%%3");

            //Assert
            Assert.AreEqual(expected, actual);
        }

    }
}
