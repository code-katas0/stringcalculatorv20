﻿using System;

namespace StringCalculatorV20
{
   public class StringCalculatorHandler:IStringCalculator
    {
        private const char slash = '/';
        private const char leftSquare = '[';
        private const char rightSquare = ']';
        private const char newLine = '\n';

        public int Add(string numbers)
        {
            var delimiterArray = GetDelimiters(numbers);

            ValidateNumbers(numbers, delimiterArray);

            return 0;
        }

        private string[] GetDelimiters(string numbers)
        {
            numbers = string.Concat(numbers.Split(slash));
            var delimiterArray = new string[] { numbers.Substring(0, numbers.IndexOf(newLine)) };

            if (numbers.Contains(leftSquare.ToString()))
            {
                delimiterArray = numbers.Substring(0, numbers.IndexOf(newLine)).Split(leftSquare, rightSquare);
            }

            return delimiterArray;
        }

        private void ValidateNumbers(string numbers, string[] delimiterArray)
        {
            var negativeNumbers = string.Empty;
            numbers = string.Concat(numbers.Split(leftSquare, slash, rightSquare, newLine));

            foreach (var number in numbers.Split(delimiterArray, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    var convertedNumber = int.Parse(number);

                    if (convertedNumber < 0)
                    {
                        negativeNumbers = string.Join(" ", negativeNumbers, number);
                    }
                }
                catch
                {
                    throw new Exception("Invalid Delimiters");
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                throw new Exception("Negatives not allowed." + negativeNumbers);
            }
        }
    }
}
