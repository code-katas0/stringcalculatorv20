﻿
namespace StringCalculatorV20
{
    public class StringCalculator : IStringCalculator
    {
        private const char comma = ',';
        private const char newLine = '\n';

        public virtual int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var total = GetTotal(numbers);

            return total;
        }

        private int GetTotal(string numbers)
        {
            var sum = 0;

            foreach (var number in numbers.Split(comma, newLine))
            {
                sum += int.Parse(number);
            }

            return sum;
        }
    }
}
