﻿using System;

namespace StringCalculatorV20
{
    public class StringCalculatorCustomDelimiter:IStringCalculator
    {
        private const char slash = '/';
        private const char leftSquare = '[';
        private const char rightSquare = ']';
        private const char newLine = '\n';

        public int Add(string numbers)
        {
            var delimiterArray = GetDelimiters(numbers);
            var total = GetTotal(numbers, delimiterArray);

            return total;
        }

        private string[] GetDelimiters(string numbers)
        {
            numbers = string.Concat(numbers.Split(slash));
            var delimiterArray = new string[] { numbers.Substring(0, numbers.IndexOf(newLine)) };

            if (numbers.Contains(leftSquare.ToString()))
            {
                delimiterArray = numbers.Substring(0, numbers.IndexOf(newLine)).Split(leftSquare, rightSquare);
            }

            return delimiterArray;
        }

        private int GetTotal(string numbers, string[] delimiterArray)
        {
            var sum = 0;

            numbers = string.Concat(numbers.Split(leftSquare, slash, rightSquare));

            foreach (var number in numbers.Split(delimiterArray, StringSplitOptions.RemoveEmptyEntries))
            {
                var convertedNumber = int.Parse(number);
                if (convertedNumber <= 1000)
                {
                    sum += convertedNumber;
                }
            }

            return sum;
        }
    }
}
